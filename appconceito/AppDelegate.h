//
//  AppDelegate.h
//  appconceito
//
//  Created by Felipe Arimateia Terra Souza on 06/04/13.
//  Copyright (c) 2013 Felipe Arimateia Terra Souza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic, getter = theNewPath) NSString *newPath;

@end
