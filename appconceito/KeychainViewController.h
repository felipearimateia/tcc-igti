//
//  KeychainViewController.h
//  appconceito
//
//  Created by Felipe Arimateia Terra Souza on 06/04/13.
//  Copyright (c) 2013 Felipe Arimateia Terra Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeychainItemWrapper.h"
#import <Security/Security.h>

@interface KeychainViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textNumberCard;
@property (weak, nonatomic) IBOutlet UITextView *textLog;

- (IBAction)actionSave:(id)sender;
- (IBAction)actionRecuperar:(id)sender;

@end
