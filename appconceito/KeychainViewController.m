//
//  KeychainViewController.m
//  appconceito
//
//  Created by Felipe Arimateia Terra Souza on 06/04/13.
//  Copyright (c) 2013 Felipe Arimateia Terra Souza. All rights reserved.
//

#import "KeychainViewController.h"

@interface KeychainViewController ()

@property (strong, nonatomic) NSString *log;

@property (strong, nonatomic) KeychainItemWrapper *wrapper;

@end

@implementation KeychainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionSave:(id)sender {
    
    [self.view endEditing:YES];
    
    [_textLog setText:nil];
    
    NSString *numberCard = [_textNumberCard text];
    
    id cardNumber = (__bridge id)kSecValueData;
    
    if (_wrapper == nil) {
        _log = @"#Criando ou recuperando Keychain MyCards\n";
        [_textLog setText:_log];
        _wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"MyCards" accessGroup:nil];
    }

    
    _log = [NSString stringWithFormat:@"%@ \n #%@",_log, @"Salvando número do cartão no Keychain.\n"];
    [_textLog setText:_log];
    
    [_wrapper setObject:numberCard forKey:cardNumber];
    
    _log = [NSString stringWithFormat:@"%@ \n #%@",_log, @"Recuperando número do cartão.\n"];
    [_textLog setText:_log];
    
    _log = [NSString stringWithFormat:@"%@ \n #%@ %@",_log, @"NÚMERO:", [_wrapper objectForKey:cardNumber]];
    [_textLog setText:_log];
    
}

- (IBAction)actionRecuperar:(id)sender {
    
    [_textLog setText:nil];
    
    id cardNumber = (__bridge id)kSecValueData;
    
    
    if (_wrapper == nil) {
        _log = @"#Criando ou recuperando Keychain MyCards\n";
        [_textLog setText:_log];
        _wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"MyCards" accessGroup:nil];
    }
    
    _log = [NSString stringWithFormat:@"%@ \n #%@",_log, @"Recuperando número do cartão.\n"];
    [_textLog setText:_log];
    
    NSString *number = [_wrapper objectForKey:cardNumber];
    
    if (number) {
        _log = [NSString stringWithFormat:@"%@ \n #%@ %@",_log, @"NÚMERO:", number];
        [_textLog setText:_log];
    }
    else {
        _log = [NSString stringWithFormat:@"%@ \n #%@",_log, @"Número não encontrado!"];
        [_textLog setText:_log];
    }
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField endEditing:YES];
    
    return YES;
    
}

@end
