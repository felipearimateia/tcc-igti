//
//  AppDelegate.m
//  appconceito
//
//  Created by Felipe Arimateia Terra Souza on 06/04/13.
//  Copyright (c) 2013 Felipe Arimateia Terra Souza. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self listDirectories];
    [self createDataBase];
    
//    [self performSelector:@selector(doReload) withObject:nil afterDelay:20];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) applicationProtectedDataDidBecomeAvailable:(UIApplication *)application {
    
}

- (void) applicationProtectedDataWillBecomeUnavailable:(UIApplication *)application {
    
}

- (void)listDirectories {
    
    
    NSLog(@"#PATH BUNDLE: %@", [[NSBundle mainBundle] resourcePath]);
    
    NSArray *arraypath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    for (NSString *path in arraypath) {
        NSLog(@"#PATH DOCUMENT: %@", path);
    }
    
    arraypath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    for (NSString *path in arraypath) {
        NSLog(@"#PATH LIBRARY: %@", path);
    }
    
    
}

- (void)createDataBase {
    
    //Gerenciar arquivos
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *nameDataBase = @"database.sqlite";
    
    //Recuperar path do banco de dados no Bundle
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:nameDataBase];
    
    NSLog(@"#PATH DATABASE: %@", databasePathFromApp);
    
    //Verifica se o arquivo existe
    if ([fileManager fileExistsAtPath:databasePathFromApp]) {
        
        //Recupera Path /Library
        NSArray *arraypath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        
        NSString *pathLibrayDocument = [arraypath objectAtIndex:0];
        
        //Cria path para onde vamos copiar o banco de dados
        self.newPath = [pathLibrayDocument stringByAppendingPathComponent:nameDataBase];
        
        NSError *error;
        
        //Copia banco do Bundle para /Library
        [fileManager copyItemAtPath:databasePathFromApp toPath:self.newPath error:&error];
        
        NSLog(@"#NEW PATH DATABASE: %@", self.newPath);
        
        
        //Seta Proteção
        BOOL sucesso = [fileManager setAttributes:@{NSFileProtectionKey: NSFileProtectionComplete} ofItemAtPath:self.newPath error:&error];
        
        if (sucesso) {
            //Recupera Tipo de Proteção
            NSString *fileProtectionValue = [[fileManager attributesOfItemAtPath:self.newPath error:&error]valueForKey:NSFileProtectionKey];
            NSLog(@"file protection value: %@", fileProtectionValue);
        }
    }
    
}

- (void)doReload {
    
    NSLog(@"Protecao Habilitada: %@",[[UIApplication sharedApplication] isProtectedDataAvailable] ? @"sim" : @"não");
    
    NSError *error;
    
    NSString *fileContents = [NSString stringWithContentsOfFile:self.newPath
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
    
    NSLog(@"file contents: %@\nError: %@", fileContents, error);
    
}

@end
