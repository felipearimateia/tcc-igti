//
//  EncryptionViewController.m
//  appconceito
//
//  Created by Felipe Arimateia Terra Souza on 21/04/13.
//  Copyright (c) 2013 Felipe Arimateia Terra Souza. All rights reserved.
//

#import "EncryptViewController.h"

@interface EncryptViewController ()

@end

@implementation EncryptViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionGetPhoto:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)actionEncryption:(id)sender {
    
    NSError *error;
    
    if (! [[CPCryptController sharedController] encryptData:UIImageJPEGRepresentation(_imageView.image, 0.9) password:@"qwert123" error:&error] ) {
        NSLog(@"Could not encrypt data: %@", error);
    };
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    _imageView.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
