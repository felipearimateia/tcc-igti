//
//  EncryptionViewController.h
//  appconceito
//
//  Created by Felipe Arimateia Terra Souza on 21/04/13.
//  Copyright (c) 2013 Felipe Arimateia Terra Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNCryptManager.h"
#import "CPCryptController.h"

@interface EncryptViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)actionGetPhoto:(id)sender;

- (IBAction)actionEncryption:(id)sender;

@end
