//
//  DecryptViewController.m
//  appconceito
//
//  Created by Felipe Arimateia Terra Souza on 21/04/13.
//  Copyright (c) 2013 Felipe Arimateia Terra Souza. All rights reserved.
//

#import "DecryptViewController.h"

@interface DecryptViewController ()

@end

@implementation DecryptViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self decrypt];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)decrypt {
    
    NSError *error;
    NSData *data = [[CPCryptController sharedController] decryptDataWithPassword:@"qwert123" error:&error];
    
    if (! data) {
        if ([error code] == kCCDecodeError) {

            [self performSegueWithIdentifier:@"showPassword" sender:self];
        }
        else {
            NSAssert(NO, @"Couldn't decrypt: %@", error);
        }
    }
    
    _imageView.image = [UIImage imageWithData:data];
    
}

@end
